const sass = require('node-sass');

module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);
  grunt.initConfig({
    sass: {
      options: {
        implementation: sass,
        sourceMap: true
      },
      dist: {
        files: {
          'css/main.css': 'sass/style.scss'
        }
      }
    },
    watch: {
      scripts: {
        files: [
          'sass/*.scss',
          'sass/**/*.scss',
          'js/*.js',
          'js/**/*.js'
        ],
        tasks: [
          'default'
        ],
        options: {
          spawn: false,
          licereload: true
        },
      },
    },
    postcss: {
      options: {
        map: {
            inline: false,
        },
        processors: [
          require('autoprefixer')({
            browsers: 'last 2 versions'
          }),
          require('cssnano')({
            zindex: false,
            reduceIdents: false
          }),
        ]
      },
      dist: {
        src: 'css/*.css'
      }
    },
    concat: {
      options: {
        seperator: ';',
        sourceMap: true
      },
      dist: {
        src: [
          'js/main.js'
        ],
        dest: 'js/global.js'
      }
    },
    notify: {
      watch: {
        options: {
          title: 'Gruntbuild',  // optional
          message: 'Ferdig!', //required
        }
      },
      server: {
        options: {
          message: 'Server is ready!'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('default', [
    'sass',
    'postcss',
    'concat',
    'notify:watch'
  ]);


};
